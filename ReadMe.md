# Running the Application #

The application is run by executing the challenge.jar file on the command line

The executable jar takes the following spring boot parameters

--source=[filename]
--mongo=[hostname]:[port]

e.g.
```
"java -jar challenge.jar --source=test.txt --mongo=localhost:27017"
```

## Reading the file/viewing the results ##

The file is inserted into the database and the word counts are used using a REST controller running on port 8080.

In order to read the file and add the counts to the DB run a POST request

i.e.
```
curl -X "POST" http://localhost:8080
```

In order to display the counts stored in the DB run a GET request

i.e.
```
curl -X "GET" http://localhost:8080
```

In order to delete all items associated with the supplied filename in the DB, run a DELETE request

i.e.
```
curl -X "DELETE" http://localhost:8080
```

## Technologies Used ##

This solution has been created using spring boot & maven to create a REST api that handles the insertion, deletion and viewing of word counts stored in a MongoDB database.  The file is being read using a Scanner of a FileInputStream so that the file can be read line by line rather than putting the entire file into memory.  Output has been sent straight to console/command line for simplicity's sake.  