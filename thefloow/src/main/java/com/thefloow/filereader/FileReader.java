/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thefloow.filereader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Pattern;

/**
 *
 * @author adaml_000
 */
public class FileReader {
    HashMap<String, Integer> words;
    
    String filePath;
    
    Pattern wordPattern;
    
    /**
     * Constructor
     * 
     * @param source 
     */
    public FileReader(String source) {
        filePath = source;
        words = new HashMap<>();
        
    }
    
    /**
     * Read the input file line by line and return word counts
     * 
     * @return the HashMap containing the count for each word
     * @throws IOException 
     */
    public HashMap<String, Integer> readFile() throws IOException {
        
        System.out.println("File Path: " + filePath);
        
        FileInputStream inputStream = null;
        Scanner sc = null;
        try {
            //read the file
            File file = new File(filePath);
            inputStream = new FileInputStream(file);
            sc = new Scanner(inputStream, "UTF-8");
            //read the file line by line
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                
                /**
                 * This is a quick way of breaking the line down word by word.
                 * 
                 * It doesn't take into account things such as punctuation or
                 * xml tags etc which could be accounted for using a regex as a
                 * future improvement.
                 */
                StringTokenizer tokenizer = new StringTokenizer(line);
                
                while (tokenizer.hasMoreTokens()) {
					String word = tokenizer.nextToken().toLowerCase();

					int val = 1;
					if (words.containsKey(word)) {
						val = words.get(word) + 1;
					}

                    // add the new/updated word count into the map
					words.put(word, val);
				}
            }
            // note that Scanner suppresses exceptions
            if (sc.ioException() != null) {
                throw sc.ioException();
            }
        } catch (Exception e){
            // this is less than ideal, but is good enough for this challenge
            System.out.println("Error: " + e.getMessage());
        } finally {
            // close the input stream and scanner
            if (inputStream != null) {
                inputStream.close();
            }
            if (sc != null) {
                sc.close();
            }
            
        }
        return words;
    }
}
