/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thefloow.filereader;

/**
 *
 * @author adaml_000
 */
public class ReadResult {
    private String file;
    private int totalWords;
    private int uniqueWords;
    private String commonWord;
    private Integer commonCount;
    private String rareWord;
    private Integer rareCount;

    /**
     * Constructor
     */
    public ReadResult() {
    }

    /**
     * Constructor
     */
    public ReadResult(String file, int totalWords, int uniqueWords, 
            String commonWord, Integer commonCount, String rareWord, 
            Integer rareCount) {
        this.file = file;
        this.totalWords = totalWords;
        this.uniqueWords = uniqueWords;
        this.commonWord = commonWord;
        this.commonCount = commonCount;
        this.rareWord = rareWord;
        this.rareCount = rareCount;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public int getTotalWords() {
        return totalWords;
    }

    public void setTotalWords(int totalWords) {
        this.totalWords = totalWords;
    }

    public int getUniqueWords() {
        return uniqueWords;
    }

    public void setUniqueWords(int uniqueWords) {
        this.uniqueWords = uniqueWords;
    }

    public String getCommonWord() {
        return commonWord;
    }

    public void setCommonWord(String commonWord) {
        this.commonWord = commonWord;
    }

    public Integer getCommonCount() {
        return commonCount;
    }

    public void setCommonCount(Integer commonCount) {
        this.commonCount = commonCount;
    }

    public String getRareWord() {
        return rareWord;
    }

    public void setRareWord(String rareWord) {
        this.rareWord = rareWord;
    }

    public Integer getRareCount() {
        return rareCount;
    }

    public void setRareCount(Integer rareCount) {
        this.rareCount = rareCount;
    }
    
    /**
     * Convert the object to a string for debugging
     * 
     * @return the String representation of the object
     */
    @Override
    public String toString() {
        return "File: " + file + ", TotalWords: " + totalWords 
                + ", UniqueWords: " + uniqueWords + ", commonWord: " 
                + commonWord + ", commonCount: " + commonCount + ", rareWord: " 
                + rareWord + ", rareCount: " + rareCount;
    }
    
}
