package com.thefloow.filereader;

import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import static com.mongodb.client.model.Filters.eq;
import com.mongodb.client.result.DeleteResult;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import org.springframework.web.bind.annotation.RequestMethod;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author adaml_000
 */
@RestController
public class FileReadController {
    //The supplied file path to load
    @Value("${source}")
    private String filePath;
    
    //The supplied mongo database details
    @Value("${mongo}")
    private String mongoValHostAndPort;
    
    //The seperated command line mongo arguments
    String[] mongoHostAndPort;
    
    //MongoDB information.
    CodecRegistry pojoCodecRegistry;
    MongoClient mongo;
    MongoDatabase database;
    MongoCollection<ReadResult> collection;
    
    //Setup the class after instantiation
    @PostConstruct
    public void Setup() {
        mongoHostAndPort = mongoValHostAndPort.split(":");

        pojoCodecRegistry = fromRegistries(MongoClient
            .getDefaultCodecRegistry(), fromProviders(
                PojoCodecProvider.builder().automatic(true).build()));
        mongo = new MongoClient(new ServerAddress(mongoHostAndPort[0], 
            Integer.parseInt(mongoHostAndPort[1])), 
            MongoClientOptions.builder().codecRegistry(pojoCodecRegistry)
                .build());
        database = mongo.getDatabase("theFloow");
        collection = database.getCollection("words", ReadResult.class);
    }
    
    /**
     * Read the supplied file and input it into the database
     * 
     * @throws IOException 
     */
    @RequestMapping(value = "/", method = RequestMethod.POST)
    public void readFile() throws IOException {
        //generate the Map of word counts
        FileReader reader = new FileReader(filePath);
        HashMap<String,Integer> wordMap = reader.readFile();
        
        Map.Entry<String, Integer> maxEntry = null;
        Map.Entry<String, Integer> minEntry = null;
        int totalWords = 0;
        int uniqueWords = wordMap.size();
        for (Map.Entry<String, Integer> entry : wordMap.entrySet()) {

            if (maxEntry == null
                    || entry.getValue().compareTo(maxEntry.getValue()) > 0) {
                maxEntry = entry;
            }
            if (minEntry == null
                    || entry.getValue().compareTo(minEntry.getValue()) < 0) {
                minEntry = entry;
            }
            
            totalWords = totalWords + entry.getValue();
        }
        
        // Add the result to the database
        if (maxEntry != null && minEntry != null) {
            System.out.println("Total Words: " + totalWords);
            System.out.println("Unique Words: " + uniqueWords);
            System.out.println("Most common word is: " + maxEntry.getKey());
            System.out.println("It appears: " + maxEntry.getValue() + " times");
            System.out.println("Least common word is: " + minEntry.getKey());
            System.out.println("It appears: " + minEntry.getValue() + " times");
            
            ReadResult readResult = new ReadResult(filePath, totalWords, 
                    uniqueWords, maxEntry.getKey(), maxEntry.getValue(), 
                    minEntry.getKey(), minEntry.getValue());
            
            collection.insertOne(readResult);
        }
    }
    
    /**
     * View Word Counts stored in the DB
     * 
     * @throws IOException 
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public void viewFile() throws IOException {
        Block<ReadResult> printBlock = new Block<ReadResult>() {
            @Override
            public void apply(final ReadResult strRes) {
                System.out.println(strRes.toString());
            }
        };

        // retrieve all items in the "words" collection and output them
        collection.find().forEach(printBlock);
    }
    
    /**
     * Delete all results from the "words" collection for the given file path
     * 
     * @throws IOException 
     */
    @RequestMapping(value = "/", method = RequestMethod.DELETE)
    public void deleteFile() throws IOException {
        DeleteResult deleteResult = collection.deleteMany(eq("file", filePath));
        System.out.println(deleteResult.getDeletedCount());
    }
}
